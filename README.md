Hello world example with Jenkins pipeline and Kubernetes deployment.


### Requirements to run out of the box on local machine
- Install Jenkins  
    - Install docker plugin
    - Configure Maven 3.6 as "M3"
    - Set up credentials for Docker Hub as [vzedano.docker]
- Install docker
- Install minikube (VBox is adviced to be installed first)
- Install JDK 8u192 (potentially, can work on any JDK8)
- Install and configure Maven 3.6.0 on the local machine

### Get started

- Reference the `Jenkinsfile` using a "Pipeline" project in Jenkins.
- To deploy to kubernetes, run `kubernetes apply -f deployments`
- Should be able to access the service via next command:
        curl http://`minikube ip`:30001/; echo
        curl http://`minikube ip`:30001/v1/math/sum/1,2,3; echo
        curl http://`minikube ip`:30001/v1/math/pow/5/2; echo

### TODO
Automate deployment to Kubernetes via Kubernetes plugin in Jenkins
