package com.challenge.api.v1.pow;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PowUtil {
	private static Log log = LogFactory.getLog(PowUtil.class);

	protected String execPow(String number, String power) throws Exception {
		int num, pow, result;
		
		try {
			log.info("Processing input number: " + number + "...");
			num = Integer.parseInt(number);
			log.info("Processing input number: " + power + "...");
			pow = Integer.parseInt(power);
			
			log.info("Executing power...");
			result = (int)Math.pow(num, pow);
		} catch (Exception e) {
			throw e;
		}
		return String.valueOf(result);
	}
}
