package com.challenge.api.v1.pow;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/v1/math/pow")
public class PowController {
	
	private static Log log = LogFactory.getLog(PowController.class);
	private PowUtil powUtil = new PowUtil();
	
	@GetMapping("/{number}/{power}")
	public String pow(
			@PathVariable("number") String number,
			@PathVariable("power") String power,
			HttpServletResponse res){
		log.info("Received a /pow request...");
		
		String result = "";
		
		try {
			result = powUtil.execPow(number, power);
		} catch (Exception e) {
			log.error("Error executing sum: " + e.getMessage(), e);
			res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return "Error processing numbers.";
		}
		
		return result;
	}	
}
