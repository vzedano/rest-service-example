package com.challenge.api.v1.sum;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SumUtil {
	private static Log log = LogFactory.getLog(SumUtil.class);

	protected String execSum(String numbers) throws Exception {
		long sum = 0;
		String[] splitNumbers = numbers.split(",");
		
		try {
			for (String stringNum : splitNumbers) {
				log.info("Processing input number: " + stringNum + "...");
				long num = Long.parseLong(stringNum);
				sum += num;
			}
		}catch (Exception e) {
			throw e;
		}
		return String.valueOf(sum);
	}
}
