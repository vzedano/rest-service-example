package com.challenge.api.v1.sum;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/v1/math/sum")
public class SumController {
	
	private static Log log = LogFactory.getLog(SumController.class);
	private SumUtil sumUtil = new SumUtil();
	
	@GetMapping("/{numbers}")
	public String sum(
			@PathVariable("numbers")String numbers, 
			HttpServletResponse res){
		log.info("Received a /sum request...");
		String sum = "";
		
		try{
			sum = sumUtil.execSum(numbers);
		} catch (Exception e) {
			log.error("Error executing sum: " + e.getMessage(), e);
			res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return "Error processing numbers.";
		}

		return String.valueOf(sum);
	}
	

}
