package com.challenge.api.v1.pow;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import com.challenge.api.v1.sum.SumController;
import com.challenge.api.v1.sum.SumTest;
import com.challenge.api.v1.sum.SumUtil;

public class PowTest {
	private static Log log = LogFactory.getLog(SumTest.class);

	
	@Test
	public void testValidInputNumbers() {
		String inputNumber1 = "5", inputPower1 = "2", expectedResult1 = "25",
				inputNumber2 = "3", inputPower2 = "4", expectedResult2 = "81";
		
		
		PowUtil powUtil = new PowUtil();
		try {
			Assert.assertEquals(expectedResult1, powUtil.execPow(inputNumber1, inputPower1));
			Assert.assertEquals(expectedResult2, powUtil.execPow(inputNumber2, inputPower2));
		} catch (Exception e){
			log.error("Unexpected error.", e);
			Assert.fail("Shouldn't throw an exception in this case.");
		}
	}
	
	
	@Test
	public void testInvalidInputNumbers() {
		String validNumber = "5", invalidNumber = "asdf",
				validPower = "2", invalidPower = "asdf";
		
		
		PowUtil powUtil = new PowUtil();
		try {
			String aSum = powUtil.execPow(validNumber, invalidPower);
			Assert.fail("Should have thrown an exception by this line. Result was: " + aSum);
		} catch (Exception e){
			
		}
		
		try {
			String aSum = powUtil.execPow(invalidNumber, validPower);
			Assert.fail("Should have thrown an exception by this line. Result was: " + aSum);
		} catch (Exception e){
			
		}
	}
}
