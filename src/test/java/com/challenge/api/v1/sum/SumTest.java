package com.challenge.api.v1.sum;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

public class SumTest {
	private static Log log = LogFactory.getLog(SumTest.class);
	
	
	@Test
	public void testValidInputNumbers() {
		String numbers1 = "1,2,3", expectedResult1 = "6",
				numbers2 = "4,5,6", expectedResult2 = "15";
		
		
		SumUtil sumUtil = new SumUtil();
		try {
			Assert.assertEquals(expectedResult1, sumUtil.execSum(numbers1));
			Assert.assertEquals(expectedResult2, sumUtil.execSum(numbers2));
		} catch (Exception e){
			log.error("Unexpected error.", e);
			Assert.fail("Shouldn't throw an exception in this case.");
		}
	}
	
	
	@Test
	public void testInvalidInputNumbers() {
		String numbers = "1,asdf,3";
		
		
		SumUtil sumUtil = new SumUtil();
		try {
			String aSum = sumUtil.execSum(numbers);
			Assert.fail("Should have thrown an exception by this line. Result was: " + aSum);
		} catch (Exception e){
			
		}
		
	}
}
